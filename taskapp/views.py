from django.shortcuts import render, redirect

# Create your views here.
from django.views.generic import *
from .models import *
from .forms import *
from django.core.mail import send_mail
from django.conf import settings
from django.contrib.auth import authenticate,login,logout
from django.urls import reverse,reverse_lazy

# Create your views here.
class HomeView(TemplateView):
	template_name="index.html"

	def dispatch(self,request,*args,**kwargs):
		if request.user.is_authenticated:
			pass
		else:
			return redirect("taskapp:login")


		return super().dispatch(request,*args,**kwargs)

	def get_context_data(self,**kwargs):
		context=super().get_context_data(**kwargs)
		context["alltasks"]=Task.objects.all()
		return context


class NepalView(TemplateView):
	template_name="nepal.html"

class AboutView(TemplateView):
	template_name="about.html"


class TaskListView(ListView):
	template_name="tasks.html"
	queryset=Task.objects.all().order_by("-id")  #queryset API
	context_object_name="alltasks"


class BlogListView(ListView):
	template_name="blog.html"
	queryset=Blog.objects.all().order_by("-id")
	context_object_name="allblogs"



class BottleListView(ListView):
	template_name="bottle.html"
	queryset=Bottle.objects.all().order_by("-id")
	context_object_name="allbottles"


class TaskDetailView(DetailView):
	template_name="taskdetail.html"
	queryset=Task.objects.all()     #model=Task
	context_object_name="taskobject"



class BlogDetailView(DetailView):
	template_name="blogdetail.html"
	queryset=Blog.objects.all()     #model=Blog
	context_object_name="blogobject"


class TaskAddView(CreateView):
	template_name="taskcreate.html"
	form_class=TaskForm
	success_url="/"




class BlogAddView(CreateView):
	template_name="blogcreate.html"
	form_class=BlogForm
	success_url="/"


class BottleAddView(CreateView):
	template_name="bottlecreate.html"
	form_class=BottleForm
	success_url="/"

class TaskUpdateView(UpdateView):
	template_name="taskcreate.html"
	form_class=TaskForm
	success_url="/task/list/"
	model=Task
	queryset=Task.objects.all()    #[optional with model]



class BlogUpdateView(UpdateView):
	template_name="blogcreate.html"
	form_class=BlogForm
	success_url="/blog/list/"
	model=Blog
	queryset=Blog.objects.all()    #[optional with model]




class TaskDeleteView(DeleteView):
	template_name="taskdelete.html"
	model=Task
	success_url="/task/list/"



class BlogDeleteView(DeleteView):
	template_name="blogdelete.html"
	model=Blog
	success_url="/blog/list/"




class LoginView(FormView):
	template_name="login.html"
	form_class=LoginForm
	success_url="/"


	def form_valid(self,form):
		x=form.cleaned_data["username"]
		y=form.cleaned_data["password"]
		usr=authenticate(username=x,password=y)
		if usr is not None:
			login(self.request,usr)

		else:
			return render(self.request,'login.html',
				{
				"error":"Invalid Credential",
				"form":form
				})

		return super().form_valid(form)

	def get_success_url(self):


		if self.request.user.is_superuser:
			return "/"

		else:
			return '/'


class SignupView(FormView):
	template_name="signup.html"
	form_class=SignupForm
	success_url="/"


	def form_valid(self,form):
		x=form.cleaned_data["username"]
		e=form.cleaned_data["email"]
		y=form.cleaned_data["password"]

		usr=User.objects.create_user(x,e,y)
		login(self.request,usr)


		return super().form_valid(form)



class LogoutView(View):
	def get(self,request):
		logout(request)
		
		return redirect('/')


