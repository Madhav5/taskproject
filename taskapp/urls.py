from django.urls import path
from .views import *


app_name="taskapp"
urlpatterns=[
path("",HomeView.as_view()),
path("nepal/",NepalView.as_view()),
path("about/",AboutView.as_view()),
path("task/list/",TaskListView.as_view()),
path("blog/list/",BlogListView.as_view()),
path("bottle/list/",BottleListView.as_view()),
path("task/<int:pk>/detail/",TaskDetailView.as_view(),name="taskdetail"),
path("blog/<int:pk>/detail/",BlogDetailView.as_view()),
path("task/add/",TaskAddView.as_view()),
path("blog/add/",BlogAddView.as_view()),
path("bottle/add/",BottleAddView.as_view()),
path("task/<int:pk>/update/",TaskUpdateView.as_view()),
path("blog/<int:pk>/update/",BlogUpdateView.as_view()),
path("task/<int:pk>/delete/",TaskDeleteView.as_view()),
path("blog/<int:pk>/delete/",BlogDeleteView.as_view()),

path("login/",LoginView.as_view(),name="login"),
path("signup/",SignupView.as_view(),name="signup"),
path("logout/",LogoutView.as_view(),name="logout"),
]