from django import forms
from .models import *
from django.contrib.auth.models import User




class TaskForm(forms.ModelForm):
	class Meta:
		model=Task
		fields=["title","description","due_date","given_to"]   #fields="__all__"



class BlogForm(forms.ModelForm):
	class Meta:
		model=Blog
		fields="__all__"



class BottleForm(forms.ModelForm):
	class Meta:
		model=Bottle
		fields="__all__"




class LoginForm(forms.Form):
	username=forms.CharField(widget=forms.TextInput(attrs={
		"class":"form-control",
		"autocomplete":"off",
		"placeholder":"Enter Username"
		}))
	password=forms.CharField(widget=forms.PasswordInput(attrs={
		'class':"form-control",
		"autocomplete":"off",
		"placeholder":"Enter password"
		}))





class SignupForm(forms.Form):
	username=forms.CharField(widget=forms.TextInput(attrs={
		"class":"form-control",
		"autocomplete":"off",
		"placeholder":"Enter Username"
		}))
	email=forms.EmailField(widget=forms.EmailInput(attrs={
		"class":"form-control",
		"autocomplete":"off",
		"placeholder":"Enter email"
		}))
	password=forms.CharField(widget=forms.PasswordInput(attrs={
		"class":"form-control",
		"autocomplete":"off",
		"placeholder":"Enter Password"
		}))
	confirm_password=forms.CharField(widget=forms.PasswordInput(attrs={
		"class":"form-control",
		"autocomplete":"off",
		"placeholder":"Enter Password"
		}))


	def clean_username(self):
		uname=self.cleaned_data['username']
		if User.objects.filter(username=uname).exists():
			raise forms.ValidationError("Username is already exist")

		
		return uname



	def clean_confirm_password(self):
		pw=self.cleaned_data['password']
		c_pw=self.cleaned_data['confirm_password']
		if (pw !=c_pw):
			raise forms.ValidationError("Password did not match.")

		return c_pw










