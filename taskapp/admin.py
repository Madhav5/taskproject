from django.contrib import admin

# Register your models here.
from .models import Task,Blog,Bottle


admin.site.register(Task)

admin.site.register(Blog)

admin.site.register(Bottle)