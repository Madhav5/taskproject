from django.db import models
from django.contrib.auth.models import User



class Task(models.Model):
	title=models.CharField(max_length=500)
	description=models.TextField()
	created_on=models.DateTimeField(auto_now_add=True)
	due_date=models.DateTimeField()
	given_to=models.ForeignKey(User, on_delete=models.CASCADE)

	def __str__(self):
		return self.title



class Blog(models.Model):
	title=models.CharField(max_length=500)
	description=models.TextField()
	author=models.CharField(max_length=50)
	published_on=models.DateTimeField(auto_now_add=True)
	
	def __str__(self):
		return self.title



class Bottle(models.Model):
	bottle_type=models.CharField(max_length=50)
	color=models.CharField(max_length=49)
	capacity_in_ml=models.PositiveIntegerField()
	height_in_cm=models.PositiveIntegerField()
	company=models.CharField(max_length=50)

	def __str__(self):
		return self.bottle_type+"("+self.color+")"
